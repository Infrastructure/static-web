var showData = $('#guadec-schedule');

sessions_url = 'https://connfa.guadec.org/api/v2/guadec2019/getSessions'

$.getJSON(sessions_url, function(sessions) {

var currentTime = moment().format('HH:mm');
//var currentTime = '23:42'
var currentDate = moment().format('DD-MM-YYYY')
//var currentDate = '23-08-2019'

      var output = '<ul class="text-left">';
      $.each(sessions.days, function(index, value) {
        //console.log(value.date);
        //console.log(currentDate);
        if (currentDate == value.date) {
        $.each(this.events, function (index, value) {
          var startTime = moment(value.from).format('HH:mm')
          var endTime = moment(value.to).format('HH:mm')
          if (currentTime >= startTime && currentTime <= endTime) {
          output += '<li>'+value.place + ': '+ `<a href="https://schedule.guadec.org/sessions/`+value.eventId+`" target="_blank">`+ value.name+'</a>'+'</li>';
          } else {
          console.log('FIXME')
        }
        });
      }
      });
        output += '</ul>';
        $(showData).html(output);
});

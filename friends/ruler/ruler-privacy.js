document.write( '<style type="text/css">' + 
		'#donation-banner { background: #59b006; background-image: linear-gradient(to bottom, #59b006 0%, #509E06 100%); color: #fff; padding: 0 10px; font-size: 12px; }' +
		'#donation-banner a { color: #fff; }' +
		'#donation-banner p { display: inline-block; }' +
		'#donation-banner img { margin-right: 5px; width: 24px; height: 24px; }' +
		'#donation-banner .donation-ruler { background: white; width: 210px; display: inline-block; padding: 2px; margin: 0 10px; }' +
		'#donation-banner .donation-percentage { background: rgb(245, 121, 0);}' +
		'#donation-banner .ninesix { margin: auto; width: 960px; }' +
		'#donation-banner .donation-button { border: 1px solid #555753; background-image: linear-gradient(to bottom, #73d216 0%, #4e9a06 100%); padding: 5px 10px; border-radius: 5px; text-decoration: none; color: #fff !important; font-weight: bold; }' +
		'</style>' )

document.write( '<div id="donation-banner">' +
			'<div class="ninesix">' +
			'<img src="https://static.gnome.org/friends/ruler/lock.png">' +
			'<p>Donate now to help make GNOME <a href="http://www.gnome.org/friends" style="margin-right: 20px;">safer than ever!</a> $20000 raised so far. $0 to go!</p>' +
			'<div class="donation-ruler"><div style="width: 100.00%;" class="donation-percentage">&nbsp;</div></div>' +
			'<a href="http://www.gnome.org/friends/" class="donation-button">Donate now!</a>' +
			'</div>' +
		'</div>' +

		'<div style="clear: both;"></div>')
